<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <title>login</title>
</head>

<body>
    <div class="container">
        <div class="row my-3 justify-content-center">
            <h1 class="text-center">Admin Login</h1>
            <div class="col-md-6 p-5 bg-success">
                <form action="logindata" class="form" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="email">UserName</label>
                        <input type="Email" id="email" class="form-control" name="email" placeholder="Enter Email">
                        <span class="text-danger">@error('email'){{$message}}@enderror</span>
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" id="password" class="form-control" name="password"
                            placeholder="Enter Password">
                            <span class="text-danger">@error('password'){{$message}}@enderror</span>
                    </div>
                    <div class="form-group my-3">
                        <button class="btn btn-danger" type="submit">Submit</button>
                    </div>
                    <div class="form-group">
                    <span>Don't have an account? </span><a class="text-danger" href="/registration">Sign Up</a><br>
                    <a class="text-danger" href="/forget">Forget your Password</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>

</html>