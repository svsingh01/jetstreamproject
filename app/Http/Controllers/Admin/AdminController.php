<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Models\Admin;

use Hash;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function login(Request $request){
        $request->validate(
            ['email'=>'required',
            'password'=>'required'
            ]
        );
        $data=admin::where(['email'=>$request->email])->first();
        if(!$data || Hash::check($request->password,$data->password))
        {
            return redirect('adminlogin');
        }
        else{
            return redirect('admin');
        }
       
    }
}
